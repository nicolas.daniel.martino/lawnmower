package sfeir.ndm;

import org.junit.jupiter.api.Test;
import sfeir.ndm.core.LawnMowerPathSolver;
import sfeir.ndm.parser.LawnMowerParser;
import sfeir.ndm.parser.LawnMowersParser;
import sfeir.ndm.parser.MapParser;
import sfeir.ndm.view.Cli;

import java.io.IOException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Unit test for simple App.
 */
public class AppTest {
    private LawnMowerPathSolver createSimpleTestEnv(String fileName) throws IOException {
        String path = "src/test/resources/" + fileName + ".cmds";
        final Cli cli = new Cli();
        final String rawCommands = cli.readFileAsString(List.of(path).toArray(new String[0]), null, null);
        return new LawnMowerPathSolver(rawCommands, new LawnMowersParser(new MapParser(), new LawnMowerParser()));
    }

    @Test
    public void testSimpleCommands() throws IOException {
        final LawnMowerPathSolver lmm = this.createSimpleTestEnv("looking_around");
        assertEquals(5, lmm.getMap().width());
        assertEquals(7, lmm.getMap().height());
        lmm.computeAllMoves();

        assertEquals("1 1 E", lmm.formatLawnMowersPosition());
    }

    @Test
    public void testExerciseExample() throws IOException {
        final LawnMowerPathSolver lmm = this.createSimpleTestEnv("given_map");
        assertEquals(5, lmm.getMap().width());
        assertEquals(5, lmm.getMap().height());
        lmm.computeAllMoves();

        assertEquals("1 3 N 5 1 E", lmm.formatLawnMowersPosition());
    }
}
