package sfeir.ndm.model;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MoveTest {
    @Test
    public void changeOrientation() {
        assertEquals(Cardinal.E, Move.computeNexOrientation(Cardinal.N, Move.D).get());
        assertEquals(Cardinal.O, Move.computeNexOrientation(Cardinal.N, Move.G).get());

        assertEquals(Cardinal.O, Move.computeNexOrientation(Cardinal.S, Move.D).get());
        assertEquals(Cardinal.E, Move.computeNexOrientation(Cardinal.S, Move.G).get());

        assertEquals(Cardinal.S, Move.computeNexOrientation(Cardinal.E, Move.D).get());
        assertEquals(Cardinal.N, Move.computeNexOrientation(Cardinal.E, Move.G).get());

        assertEquals(Cardinal.N, Move.computeNexOrientation(Cardinal.O, Move.D).get());
        assertEquals(Cardinal.S, Move.computeNexOrientation(Cardinal.O, Move.G).get());
    }

    @Test
    public void stepOnce() {
        assertEquals(new Position(1,2), Move.computeNextPosition(new Position(1,1), Cardinal.N));
        assertEquals(new Position(1,0), Move.computeNextPosition(new Position(1,1), Cardinal.S));
        assertEquals(new Position(2,1), Move.computeNextPosition(new Position(1,1), Cardinal.E));
        assertEquals(new Position(0,1), Move.computeNextPosition(new Position(1,1), Cardinal.O));
    }
}