package sfeir.ndm.view;

import org.junit.jupiter.api.Test;

import java.io.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CliTest {
    @Test
    public void readFileAsStringInteractiveCli() throws IOException {
        Reader in = new StringReader("./src/test/resources/looking_around.cmds");
        Writer wr = new StringWriter();

        Cli cli = new Cli();
        String commands = cli.readFileAsString(null, in, wr);

        assertEquals("5 7 1 1 E DGDGDGDG", commands);
    }

    @Test
    public void readFileAsStringWithArg() throws IOException {
        Reader in = new StringReader("");
        Writer wr = new StringWriter();

        Cli cli = new Cli();
        String commands = cli.readFileAsString(new String[]{"./src/test/resources/looking_around.cmds"}, in, wr);

        assertEquals("5 7 1 1 E DGDGDGDG", commands);
    }
}
