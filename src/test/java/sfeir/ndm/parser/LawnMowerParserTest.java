package sfeir.ndm.parser;

import org.junit.jupiter.api.Test;
import sfeir.ndm.model.*;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class LawnMowerParserTest {
    @Test
    public void parseLawnMowerWithSpaces(){
        final String commands = "1 2 N GA";
        Parser<LawnMower> lawnMowerParser = new LawnMowerParser();

        assertEquals(new LawnMower(new Position(1,2), Cardinal.N, Arrays.asList(Move.G, Move.A)), lawnMowerParser.parse(commands));
    }
}
