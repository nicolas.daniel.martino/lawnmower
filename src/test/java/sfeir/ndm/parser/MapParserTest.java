package sfeir.ndm.parser;

import org.junit.jupiter.api.Test;
import sfeir.ndm.model.Map;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class MapParserTest {
    @Test
    public void parseMapWithSpaces(){
        final String commands = "5 6 1 2 N";
        Parser<Map> mapParser = new MapParser();

        assertEquals(new Map(5, 6), mapParser.parse(commands));
    }

    @Test
    public void parseMapWithLineReturn(){
        final String commands = "5 6\n1 2 N\n";
        Parser<Map> mapParser = new MapParser();

        assertEquals(new Map(5, 6), mapParser.parse(commands));
    }
}
