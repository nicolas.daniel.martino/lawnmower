package sfeir.ndm.parser;

import org.javatuples.Pair;
import org.junit.jupiter.api.Test;
import sfeir.ndm.model.Cardinal;
import sfeir.ndm.model.LawnMower;
import sfeir.ndm.model.Map;
import sfeir.ndm.model.Position;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class LawnMowersParserTest {
    @Test
    public void parseMapWithImmobileMowerWithSpaces() {
        final String commands = "5 6 1 2 N";
        LawnMowersParser lmp = new LawnMowersParser(new MapParser(), new LawnMowerParser());
        Pair<Map, List<LawnMower>> result = lmp.parse(commands);

        assertEquals(new Map(5,6), result.getValue0());
        assertEquals(new Position(1,2), result.getValue1().get(0).getPosition());
        assertEquals(Cardinal.N, result.getValue1().get(0).getOrientation());
        assertEquals(new ArrayList<>(), result.getValue1().get(0).getMoves());
    }

    @Test
    public void parseMapWithImmobileMowerWithLineBreaks() {
        final String commands = "5 6\n1 2 N\n";
        LawnMowersParser lmp = new LawnMowersParser(new MapParser(), new LawnMowerParser());
        Pair<Map, List<LawnMower>> result = lmp.parse(commands);

        assertEquals(new Map(5,6), result.getValue0());
        assertEquals(new Position(1,2), result.getValue1().get(0).getPosition());
        assertEquals(Cardinal.N, result.getValue1().get(0).getOrientation());
        assertEquals(new ArrayList<>(), result.getValue1().get(0).getMoves());
    }

    @Test
    public void parseMapWithSpaces() {
        final String commands = "5 6 1 2 N DGDGDGDG";
        LawnMowersParser lmp = new LawnMowersParser(new MapParser(), new LawnMowerParser());
        Pair<Map, List<LawnMower>> result = lmp.parse(commands);

        assertEquals(new Map(5,6), result.getValue0());
        assertEquals(new Position(1,2), result.getValue1().get(0).getPosition());
        assertEquals(Cardinal.N, result.getValue1().get(0).getOrientation());
        assertEquals("DGDGDGDG", result.getValue1().get(0).getMoves().stream().map(Objects::toString).collect(Collectors.joining("")));
    }

    @Test
    public void parseMapWithLineReturns() {
        final String commands = "5 6\n1 2 N\nDGDGDGDG";
        LawnMowersParser lmp = new LawnMowersParser(new MapParser(), new LawnMowerParser());
        Pair<Map, List<LawnMower>> result = lmp.parse(commands);

        assertEquals(new Map(5,6), result.getValue0());
        assertEquals(new Position(1,2), result.getValue1().get(0).getPosition());
        assertEquals(Cardinal.N, result.getValue1().get(0).getOrientation());
        assertEquals("DGDGDGDG", result.getValue1().get(0).getMoves().stream().map(Objects::toString).collect(Collectors.joining("")));
    }

    @Test
    public void parseMapMowersWithSpaces() {
        final String commands = "5 6 1 2 N DGDGDGDG 3 3 E GDGD 0 0 S D";
        LawnMowersParser lmp = new LawnMowersParser(new MapParser(), new LawnMowerParser());
        Pair<Map, List<LawnMower>> result = lmp.parse(commands);

        assertEquals(new Map(5,6), result.getValue0());
        // first lawn mower
        assertEquals(new Position(1,2), result.getValue1().get(0).getPosition());
        assertEquals(Cardinal.N, result.getValue1().get(0).getOrientation());
        assertEquals("DGDGDGDG", result.getValue1().get(0).getMoves().stream().map(Objects::toString).collect(Collectors.joining("")));

        // second lawn mower
        assertEquals(new Position(3,3), result.getValue1().get(1).getPosition());
        assertEquals(Cardinal.E, result.getValue1().get(1).getOrientation());
        assertEquals("GDGD", result.getValue1().get(1).getMoves().stream().map(Objects::toString).collect(Collectors.joining("")));

        // third lawn mower
        assertEquals(new Position(0,0), result.getValue1().get(2).getPosition());
        assertEquals(Cardinal.S, result.getValue1().get(2).getOrientation());
        assertEquals("D", result.getValue1().get(2).getMoves().stream().map(Objects::toString).collect(Collectors.joining("")));
    }

    @Test
    public void parseMapMowersWithLineBreaks() {
        final String commands = "5 6\n1 2 N\nDGDGDGDG\n3 3 E\nGDGD\n0 0 S\nA";
        LawnMowersParser lmp = new LawnMowersParser(new MapParser(), new LawnMowerParser());
        Pair<Map, List<LawnMower>> result = lmp.parse(commands);

        assertEquals(new Map(5,6), result.getValue0());
        // first lawn mower
        assertEquals(new Position(1,2), result.getValue1().get(0).getPosition());
        assertEquals(Cardinal.N, result.getValue1().get(0).getOrientation());
        assertEquals("DGDGDGDG", result.getValue1().get(0).getMoves().stream().map(Objects::toString).collect(Collectors.joining("")));

        // second lawn mower
        assertEquals(new Position(3, 3), result.getValue1().get(1).getPosition());
        assertEquals(Cardinal.E, result.getValue1().get(1).getOrientation());
        assertEquals("GDGD", result.getValue1().get(1).getMoves().stream().map(Objects::toString).collect(Collectors.joining("")));

        // third lawn mower
        assertEquals(new Position(0, 0), result.getValue1().get(2).getPosition());
        assertEquals(Cardinal.S, result.getValue1().get(2).getOrientation());
        assertEquals("A", result.getValue1().get(2).getMoves().stream().map(Objects::toString).collect(Collectors.joining("")));
    }
}
