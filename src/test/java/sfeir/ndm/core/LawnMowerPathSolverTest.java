package sfeir.ndm.core;

import org.junit.jupiter.api.Test;
import sfeir.ndm.model.*;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class LawnMowerPathSolverTest {
    @Test
    public void mowerShouldMoveOnce() {
        LawnMower lawnMower = new LawnMower(new Position(0, 0), Cardinal.N, List.of(Move.A, Move.D));
        LawnMowerPathSolver lawnMowerPathSolver = new LawnMowerPathSolver(new Map(5, 7), List.of(lawnMower));
        lawnMowerPathSolver.computeAllMoves();

        List<LawnMower> lawnMowers = lawnMowerPathSolver.getLawnMowers();

        assertEquals(new Position(0, 1), lawnMowers.get(0).getPosition());
        assertEquals(Cardinal.E, lawnMowers.get(0).getOrientation());
        assertEquals("0 1 E", lawnMowerPathSolver.formatLawnMowersPosition());
    }

    @Test
    public void mowerShouldMoveFullCircle() {
        LawnMower lawnMower = new LawnMower(new Position(0, 0), Cardinal.N, List.of(Move.A, Move.D, Move.A, Move.D, Move.A, Move.D, Move.A));
        LawnMowerPathSolver lawnMowerPathSolver = new LawnMowerPathSolver(new Map(5, 7), List.of(lawnMower));
        lawnMowerPathSolver.computeAllMoves();

        List<LawnMower> lawnMowers = lawnMowerPathSolver.getLawnMowers();

        assertEquals(new Position(0, 0), lawnMowers.get(0).getPosition());
        assertEquals(Cardinal.O, lawnMowers.get(0).getOrientation());
        assertEquals("0 0 O", lawnMowerPathSolver.formatLawnMowersPosition());
    }

    @Test
    public void mowerShouldNotGoOutOfMap() {
        LawnMower lawnMower = new LawnMower(new Position(0, 0), Cardinal.N, List.of(Move.G, Move.A));
        LawnMowerPathSolver lawnMowerPathSolver = new LawnMowerPathSolver(new Map(5, 7), List.of(lawnMower));
        lawnMowerPathSolver.computeAllMoves();

        List<LawnMower> lawnMowers = lawnMowerPathSolver.getLawnMowers();

        assertEquals(new Position(0, 0), lawnMowers.get(0).getPosition());
        assertEquals(Cardinal.O, lawnMowers.get(0).getOrientation());
        assertEquals("0 0 O", lawnMowerPathSolver.formatLawnMowersPosition());
    }

    @Test
    public void mowerShouldNotCollideWithAnotherOne() {
        LawnMower lawnMower1 = new LawnMower(new Position(0, 0), Cardinal.N, List.of(Move.A, Move.D, Move.A));
        LawnMower lawnMower2 = new LawnMower(new Position(2, 2), Cardinal.S, List.of(Move.A, Move.D, Move.A));

        LawnMowerPathSolver lawnMowerPathSolver = new LawnMowerPathSolver(new Map(5, 7), List.of(lawnMower1, lawnMower2));
        lawnMowerPathSolver.computeAllMoves();

        List<LawnMower> lawnMowers = lawnMowerPathSolver.getLawnMowers();

        assertEquals(new Position(1, 1), lawnMowers.get(0).getPosition());
        assertEquals(Cardinal.E, lawnMowers.get(0).getOrientation());
        assertEquals(new Position(2, 1), lawnMowers.get(1).getPosition());
        assertEquals(Cardinal.O, lawnMowers.get(1).getOrientation());
        assertEquals("1 1 E 2 1 O", lawnMowerPathSolver.formatLawnMowersPosition());
        assertEquals("0 1 N\n0 1 E\n1 1 E\n2 1 S\n2 1 O\n2 1 O", String.join("\n", lawnMowerPathSolver.getMoveHistory()));
    }

    @Test
    public void mowerShouldBeAbleToGoThroughEachOther() {
        LawnMower lawnMower1 = new LawnMower(new Position(0, 0), Cardinal.N, List.of(Move.A, Move.A));
        LawnMower lawnMower2 = new LawnMower(new Position(0, 2), Cardinal.S, List.of(Move.A, Move.A));

        LawnMowerPathSolver lawnMowerPathSolver = new LawnMowerPathSolver(new Map(5, 7), List.of(lawnMower1, lawnMower2), false);
        lawnMowerPathSolver.computeAllMoves();

        List<LawnMower> lawnMowers = lawnMowerPathSolver.getLawnMowers();

        assertEquals(new Position(0, 2), lawnMowers.get(0).getPosition());
        assertEquals(Cardinal.N, lawnMowers.get(0).getOrientation());
        assertEquals(new Position(0, 0), lawnMowers.get(1).getPosition());
        assertEquals(Cardinal.S, lawnMowers.get(1).getOrientation());
        assertEquals("0 2 N 0 0 S", lawnMowerPathSolver.formatLawnMowersPosition());
    }
}
