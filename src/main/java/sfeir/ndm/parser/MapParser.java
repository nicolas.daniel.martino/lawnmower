package sfeir.ndm.parser;

import sfeir.ndm.model.Map;

public class MapParser implements Parser<Map> {
    @Override
    public Map parse(String s) {
        return new Map(Integer.parseInt(s.substring(0, 1)), Integer.parseInt(s.substring(2, 3)));
    }
}
