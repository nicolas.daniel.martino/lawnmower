package sfeir.ndm.parser;

public interface Parser<T> {
    T parse(String s);
}
