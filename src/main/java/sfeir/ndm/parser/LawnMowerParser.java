package sfeir.ndm.parser;

import sfeir.ndm.model.Cardinal;
import sfeir.ndm.model.LawnMower;
import sfeir.ndm.model.Move;
import sfeir.ndm.model.Position;

import java.util.ArrayList;
import java.util.List;

public class LawnMowerParser implements Parser<LawnMower> {
    @Override
    public LawnMower parse(String s) {
        Position position = new Position(s.substring(0, 1), s.substring(2, 3));
        Cardinal orientation = Cardinal.valueOf(s.substring(4, 5));
        List<Move> moves = new ArrayList<>();

        if (s.length() >= 6) {
            moves = s.substring(6)
                    .chars()
                    .filter(item -> (char) item != ' ')
                    .mapToObj(item -> Move.valueOf(String.valueOf((char) item)))
                    .toList();
        }

        return new LawnMower(position, orientation, moves);
    }
}
