package sfeir.ndm.parser;

import org.javatuples.Pair;
import sfeir.ndm.model.LawnMower;
import sfeir.ndm.model.Map;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LawnMowersParser implements Parser<Pair<Map, List<LawnMower>>> {
    private final Parser<Map> mapParser;
    private final Parser<LawnMower> landMowerParser;


    public LawnMowersParser(Parser<Map> mapParser, Parser<LawnMower> landMowerParser) {
        this.mapParser = mapParser;
        this.landMowerParser = landMowerParser;
    }

    public Pair<Map, List<LawnMower>> parse(String s) {
        return Pair.with(mapParser.parse(s), parseMowers(s.substring(4)));
    }

    private List<LawnMower> parseMowers(String s) {
        String lawnMowerAsString = this.transformLineReturnToSpace(s);
        final String regex = "(\\d\\s\\d\\D+)";
        final Pattern pattern = Pattern.compile(regex, Pattern.MULTILINE);
        final Matcher matcher = pattern.matcher(lawnMowerAsString);

        final List<LawnMower> lawnMowers = new ArrayList<>();
        while (matcher.find()) {
            for (int i = 1; i <= matcher.groupCount(); i++) {
                lawnMowers.add(landMowerParser.parse(matcher.group(i)));
            }
        }
        return lawnMowers;
    }

    private String transformLineReturnToSpace(String s) {
        return s.replace(System.lineSeparator(), " ");
    }
}
