package sfeir.ndm.view;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Scanner;

public class Cli {
    public String readFileAsString(String[] args, Reader userInput, Writer messageStream) throws IOException {

        if (args == null || args.length == 0) {
            return Files.readString(getPathInteractively(userInput, messageStream).toPath());
        }

        return Files.readString(Path.of(args[0]));
    }

    public File getPathInteractively(Reader userInput, Writer messageStream) throws IOException {
        try (final Scanner scanner = new Scanner(userInput)) {

            messageStream.append("""
                                        
                    Please provide a path to a command file
                    For example: ./src/test/resources/looking_around.cmds
                                    
                    """);

            messageStream.flush();
            final String input = scanner.nextLine();

            final Path path = Path.of(input);

            messageStream.append("You have entered: ")
                    .append(input)
                    .append(System.lineSeparator())
                    .append("Extracting file from path: ")
                    .append(String.valueOf(path.getFileName()));
            messageStream.flush();


            final File curFile = path.toFile();
            if (curFile.exists() && curFile.isFile()) {
                return curFile;
            }
            messageStream
                    .append(System.lineSeparator())
                    .append(System.lineSeparator())
                    .append("Error importing file: ")
                    .append(curFile.getAbsolutePath())
                    .append(" is not a file")
                    .append(System.lineSeparator())
                    .append("Please provide a path similar to: C:\\Users\\youName\\Documents\\carbon\\test1.map.txt");

            return this.getPathInteractively(userInput, messageStream);
        } finally {
            messageStream.flush();
        }
    }
}
