package sfeir.ndm.core;

import org.javatuples.Pair;
import sfeir.ndm.model.*;
import sfeir.ndm.parser.Parser;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class LawnMowerPathSolver {
    private final Map map;
    private final List<LawnMower> lawnMowers;
    private Boolean activateCollisions = true;
    private final List<String> moveHistory = new ArrayList<>();

    public LawnMowerPathSolver(Map map, List<LawnMower> lawnMowers, Boolean activateCollisions) {
        this.map = map;
        this.lawnMowers = lawnMowers;
        this.activateCollisions = activateCollisions;
    }

    public LawnMowerPathSolver(Map map, List<LawnMower> lawnMowers) {
        this.map = map;
        this.lawnMowers = lawnMowers;
    }

    public LawnMowerPathSolver(String rawInputFile, Parser<Pair<Map, List<LawnMower>>> parser) {
        final var parsed = parser.parse(rawInputFile);
        this.map = parsed.getValue0();
        this.lawnMowers = parsed.getValue1();
    }

    public void computeAllMoves() {
        for (final LawnMower lawnMower : this.lawnMowers) {
            while (lawnMower.hasNextMove()) {
                computeNextMove(lawnMower);
                this.moveHistory.add(lawnMower.toString());
            }
        }
    }

    private void computeNextMove(LawnMower lawnMower) {
        final Pair<Optional<Position>, Optional<Cardinal>> nextPositionAndOrientation = this.computeNextPositionAndOrientation(lawnMower);
        if (nextPositionAndOrientation.getValue0().isPresent() && !this.mayCollide(nextPositionAndOrientation.getValue0().get())) {
            lawnMower.setPosition(nextPositionAndOrientation.getValue0().get());
        }

        if (nextPositionAndOrientation.getValue1().isPresent()) {
            lawnMower.setOrientation(nextPositionAndOrientation.getValue1().get());
        }
    }

    private Pair<Optional<Position>, Optional<Cardinal>> computeNextPositionAndOrientation(LawnMower lawnMower) {
        Optional<Move> move = lawnMower.getNextMove();
        if (move.isEmpty()) {
            return Pair.with(Optional.empty(), Optional.empty());
        }

        final Optional<Cardinal> nextOrientation;
        final Optional<Position> nextPosition;
        switch (move.get()) {
            case A -> {
                var prospectiveNextPosition = Move.computeNextPosition(lawnMower.getPosition(), lawnMower.getOrientation());
                nextPosition = isOutOfBonds(prospectiveNextPosition) ? Optional.empty() : Optional.of(prospectiveNextPosition);
                nextOrientation = Optional.empty();
            }
            case D, G -> {
                nextPosition = Optional.empty();
                nextOrientation = Move.computeNexOrientation(lawnMower.getOrientation(), move.get());
            }
            default -> throw new IllegalStateException("Unexpected move: " + move);
        }

        return Pair.with(nextPosition, nextOrientation);
    }

    private Boolean isOutOfBonds(Position position) {
        return position.getX() < 0 || position.getX() > this.map.width() || position.getY() < 0 || position.getY() > this.map.height();
    }

    private Boolean mayCollide(Position nextPosition) {
        if (!this.activateCollisions) {
            return false;
        }

        int lawnMowerAtProspectivePosition = 0;
        for (LawnMower lawnMower : this.lawnMowers) {
            if (lawnMower.getPosition().equals(nextPosition)) {
                lawnMowerAtProspectivePosition += 1;
            }
        }

        return lawnMowerAtProspectivePosition != 0;
    }

    public String formatLawnMowersPosition() {
        return lawnMowers.stream().map(LawnMower::toString).collect(Collectors.joining(" "));
    }

    public Map getMap() {
        return map;
    }

    public List<LawnMower> getLawnMowers() {
        return lawnMowers;
    }

    public List<String> getMoveHistory() {
        return moveHistory;
    }
}
