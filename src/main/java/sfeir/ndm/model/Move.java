package sfeir.ndm.model;

import java.util.Optional;

public enum Move {
    A, // go straight one tile
    G, // change orientation to the left
    D // changes orientation to the right
    ;

    public static Optional<Cardinal> computeNexOrientation(Cardinal orientation, Move move) {
        return switch (orientation) {
            case N -> switch (move) {
                case D -> Optional.of(Cardinal.E);
                case G -> Optional.of(Cardinal.O);
                default -> Optional.empty();
            };
            case S -> switch (move) {
                case D -> Optional.of(Cardinal.O);
                case G -> Optional.of(Cardinal.E);
                default -> Optional.empty();
            };
            case E -> switch (move) {
                case D -> Optional.of(Cardinal.S);
                case G -> Optional.of(Cardinal.N);
                default -> Optional.empty();
            };
            case O -> switch (move) {
                case D -> Optional.of(Cardinal.N);
                case G -> Optional.of(Cardinal.S);
                default -> Optional.empty();
            };
        };
    }

    public static Position computeNextPosition(Position initialPosition, Cardinal orientation) {
        return new Position(initialPosition.getX() + orientation.getDirection()[0], initialPosition.getY() + orientation.getDirection()[1]);
    }
}
