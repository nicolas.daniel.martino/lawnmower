package sfeir.ndm.model;

public enum Cardinal {
    //@formatter:off
    N(new int[]{ 0, 1}),
    S(new int[]{ 0, -1}),
    E(new int[]{ 1, 0}),
    O(new int[]{ -1, 0});
    //@formatter:on
    private final int[] direction;

    Cardinal(int[] direction) {
        this.direction = direction;
    }

    public int[] getDirection() {
        return this.direction;
    }
}
