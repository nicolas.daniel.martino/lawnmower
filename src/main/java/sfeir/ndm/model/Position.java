package sfeir.ndm.model;

public record Position(int x, int y) {

    public Position(String x, String y) {
        this(Integer.parseInt(x), Integer.parseInt(y));
    }


    public int getX() {
        return this.x;
    }

    public int getY() {
        return this.y;
    }
}
