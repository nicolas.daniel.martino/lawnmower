package sfeir.ndm.model;

public record Map(int width, int height) {}
