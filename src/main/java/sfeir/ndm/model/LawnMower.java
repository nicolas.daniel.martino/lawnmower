package sfeir.ndm.model;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class LawnMower {
    private final List<Move> moves;
    private Position position;
    private Cardinal orientation;
    private Integer lastMoveIndex = 0;

    public LawnMower(Position position, Cardinal orientation, List<Move> moves) {
        this.position = position;
        this.orientation = orientation;
        this.moves = moves;
    }

    public Optional<Move> getNextMove() {
        final int nextMoveIndex = this.lastMoveIndex + 1;
        if (!this.hasNextMove()) {
            return Optional.empty();
        }

        Move nextMove = this.moves.get(this.lastMoveIndex);
        this.lastMoveIndex = nextMoveIndex;
        return Optional.ofNullable(nextMove);

    }

    public Boolean hasNextMove() {
        return this.moves.size() != this.lastMoveIndex;
    }


    public List<Move> getMoves() {
        return moves;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public Cardinal getOrientation() {
        return orientation;
    }

    public void setOrientation(Cardinal orientation) {
        this.orientation = orientation;
    }

    @Override
    public String toString() {
        return this.position.getX() + " " + this.position.getY() + " " + this.orientation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LawnMower lawnMower)) return false;

        if (!Objects.equals(position, lawnMower.position)) return false;
        if (orientation != lawnMower.orientation) return false;
        if (!Objects.equals(moves, lawnMower.moves)) return false;
        return Objects.equals(lastMoveIndex, lawnMower.lastMoveIndex);
    }

    @Override
    public int hashCode() {
        int result = position != null ? position.hashCode() : 0;
        result = 31 * result + (orientation != null ? orientation.hashCode() : 0);
        result = 31 * result + (moves != null ? moves.hashCode() : 0);
        result = 31 * result + (lastMoveIndex != null ? lastMoveIndex.hashCode() : 0);
        return result;
    }
}
