package sfeir.ndm;

import sfeir.ndm.core.LawnMowerPathSolver;
import sfeir.ndm.parser.LawnMowerParser;
import sfeir.ndm.parser.LawnMowersParser;
import sfeir.ndm.parser.MapParser;
import sfeir.ndm.view.Cli;

import java.io.*;

public class App {

    public static void main(String[] args) throws IOException {
        Cli cli = new Cli();
        Reader userInput = new BufferedReader(new InputStreamReader(System.in));
        Writer messageStream = new BufferedWriter(new OutputStreamWriter(System.out));

        messageStream.append("""
                                
                Welcome to lawn mower path solver
                                
                """);
        messageStream.flush();

        String lawnMowersCommands = cli.readFileAsString(args, userInput, messageStream);
        messageStream.append(System.lineSeparator())
                .append("The file contains the following instructions: ")
                .append(lawnMowersCommands);


        LawnMowerPathSolver lawnMowerPathSolver = new LawnMowerPathSolver(lawnMowersCommands, new LawnMowersParser(new MapParser(), new LawnMowerParser()));
        lawnMowerPathSolver.computeAllMoves();

        messageStream.append(System.lineSeparator())
                .append("Finished working with the following result: ")
                .append(System.lineSeparator())
                .append(lawnMowerPathSolver.formatLawnMowersPosition())
                .append(System.lineSeparator());


        messageStream.flush();
        messageStream.close();
        userInput.close();
    }
}
