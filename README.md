# Lawn mower exercise

## Compile with maven wrapper

./mvnw clean compile

## Run with maven wrapper

### Interactive mode

./mvnw exec:java -Dexec.mainClass=sfeir.ndm.App

### Cmd line argument mode

./mvnw exec:java -Dexec.mainClass=sfeir.ndm.App -Dexec.args="./src/test/resources/given_map.cmds"